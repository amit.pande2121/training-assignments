public class Engine {

    private boolean isPetrol;
    private int numberOfCylinders;

    public void start() {
        System.out.println("Starting Engine");
    }

    public void stop() {
        System.out.println("Stopping Engine");
    }
}
