import org.testng.Assert;
import org.testng.annotations.Test;

public class CalculatorTests {

    @Test
    public void shouldAddTwoNumbers() {
        int sum = new Calculator().getSum(10, 20);
        Assert.assertEquals(sum, 30);
    }

    @Test
    public void shouldMultiplyTwoNumbers() {
        int result = new Calculator().getResultOfMultiplication(10, 20);
        Assert.assertEquals(result, 200);
    }
}
