package fruits;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Bowl {
    private List<Fruit> fruits;
    private ShapeOfBowl shape;

    public Bowl() {
        fruits = new ArrayList<>();
    }

    public void addFruit(Fruit... fruitsList) {
        fruits.addAll(Arrays.asList(fruitsList));
    }

    public Bowl getAllApplesBowl() {

        Bowl bowlOfApples = new Bowl();

        for (Fruit fruit : fruits) {
            if (fruit instanceof Apple) {
                bowlOfApples.addFruit(fruit);
            }
        }
        return bowlOfApples;
    }

    public Bowl getAllOrangesBowl() {

        Bowl bowlOfOrange = new Bowl();

        for (Fruit fruit : fruits) {
            if (fruit instanceof Orange) {
                bowlOfOrange.addFruit(fruit);
            }
        }
        return bowlOfOrange;
    }

    public Bowl getAllBananasBowl() {
        Bowl bowlOfBanana = new Bowl();

        for (Fruit fruit : fruits) {
            if (fruit instanceof Banana) {
                bowlOfBanana.addFruit(fruit);
            }
        }
        return bowlOfBanana;
    }

    public int getCount() {
        return fruits.size();
    }
}